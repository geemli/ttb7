package com.epam.ttb7.service;

import com.epam.ttb7.entity.RoleEntity;
import com.epam.ttb7.entity.UserEntity;

public interface RoleService {
    RoleEntity findByName(String role);

    Iterable<RoleEntity> findAll();

    Iterable<RoleEntity> getRolesForUserRegister(UserEntity user);

}
