package com.epam.ttb7.service;


import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;

public interface BookingService {
    TicketEntity bookTicket(TicketEntity ticket);
    Iterable<TicketEntity> getPurchasedTickets(SessionEntity session);
}
