package com.epam.ttb7.service;

import com.epam.ttb7.entity.AuditoriumEntity;

public interface AuditoriumService  {

    Iterable<AuditoriumEntity> getAllAuditoriums();
    AuditoriumEntity getByName(String name);
}
