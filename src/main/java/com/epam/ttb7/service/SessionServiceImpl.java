package com.epam.ttb7.service;

import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.repository.AuditoriumRepository;
import com.epam.ttb7.repository.EventRepository;
import com.epam.ttb7.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private AuditoriumRepository auditoriumRepository;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public SessionEntity addSession(SessionEntity sessionEntity) {
        return sessionRepository.save(sessionEntity);
    }

    @Override
    public Iterable<SessionEntity> getSessions() {
        return sessionRepository.findAll();
    }

    @Override
    public SessionEntity getSession(int id) {
        return sessionRepository.findOne(id);
    }

    @PostConstruct
    public void init() {
        SessionEntity session = new SessionEntity();
        session.setDatetime(LocalDateTime.now());
        session.setAuditorium(auditoriumRepository.findByNameIgnoreCase("MOCKBA"));
        session.setEvent(eventRepository.findByNameIgnoreCase("star wars"));
        sessionRepository.save(session);

        SessionEntity session2 = new SessionEntity();
        session2.setDatetime(LocalDateTime.now());
        session2.setAuditorium(auditoriumRepository.findByNameIgnoreCase("MOCKBA"));
        session2.setEvent(eventRepository.findByNameIgnoreCase("harry potter"));
        sessionRepository.save(session2);

        SessionEntity session3 = new SessionEntity();
        session3.setDatetime(LocalDateTime.now());
        session3.setAuditorium(auditoriumRepository.findByNameIgnoreCase("MOCKBA"));
        session3.setEvent(eventRepository.findByNameIgnoreCase("lord of the ring"));
        sessionRepository.save(session3);
    }
}
