package com.epam.ttb7.service;

import com.epam.ttb7.entity.AuditoriumEntity;
import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.entity.UserEntity;
import com.epam.ttb7.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    TicketRepository ticketRepository;

    private final static double RATE_VIP = 1.2;
    private final static double RATE_HIGH_RATING = 2;
    private final static double RATE_10TH_TICKET = 0.5;
    private final static double RATE_BIRTHDAY = 0.95;

    @Override
    public BigDecimal getTicketsPrice(UserEntity user) {
        BigDecimal price = new BigDecimal(0);
        Iterable<TicketEntity> tickets = ticketRepository.findByPurchasedFalseAndUserIs(user);
//
        int i = 0;
        for (TicketEntity ticket : tickets) {
            if (++i % 10 == 0) {
                price = price.add(BigDecimal.valueOf(ticket.getPrice() * RATE_10TH_TICKET));
            } else {
                LocalDate birthday = ticket.getUser().getBirthday();
                LocalDate eventDay = ticket.getSession().getDatetime().toLocalDate();

                if (birthday != null &&
                        eventDay != null &&
                        eventDay.compareTo(birthday.minusDays(5)) > 0 &&
                        eventDay.compareTo(birthday.plusDays(5)) < 0) {
                    price = price.add(BigDecimal.valueOf(ticket.getPrice() * RATE_BIRTHDAY));
                } else {
                    price = price.add(BigDecimal.valueOf(ticket.getPrice()));
                }
            }


//            double currentTicketPrice = ticket.getSession().getEvent().getPrice();
//            String vipseats = ticket.getSession().getAuditorium().getVipseats();
////            IntStream streamFromString = vipseats.split("[, ]");
//            HashSet<String> hashSet = new HashSet<>(Arrays.asList(vipseats.split("[, ]")));
//
//            Set<Integer> collect = parseVips(vipseats);
////            Set<Integer> collect = Stream.of(vipseats.split("[, ]"))
////                    .map(Integer::parseInt)
////                    .collect(Collectors.toSet());
//
//            if (collect.contains(ticket.getSeat())) {
//                currentTicketPrice *= RATE_VIP;
//            }
//
//            boolean highRating = ticket.getSession().getEvent().getRating().equalsIgnoreCase("HIGH");
//            if (highRating) {
//                currentTicketPrice *= RATE_HIGH_RATING;
//            }
//            price = price.add(BigDecimal.valueOf(currentTicketPrice));
        }
        return price;
    }

    @Override
    public Double getTicketRatePrice(TicketEntity ticket) {
        double currentTicketPrice = ticket
                .getSession()
                .getEvent()
                .getPrice();

        String vipseats = ticket
                .getSession()
                .getAuditorium()
                .getVipseats();

        HashSet<String> hashSet = new HashSet<>(Arrays.asList(vipseats.split("[, ]")));

        Set<Integer> collect = parseVips(vipseats);

        if (collect.contains(ticket.getSeat())) {
            currentTicketPrice *= RATE_VIP;
        }

        boolean highRating = ticket
                .getSession()
                .getEvent()
                .getRating()
                .equalsIgnoreCase("HIGH");

        if (highRating) {
            currentTicketPrice *= RATE_HIGH_RATING;
        }
//        ticket.setPrice(currentTicketPrice);
        return currentTicketPrice;
    }

    @Override
    public Map<Integer, Double> getTicketsPrice(SessionEntity session) {
        Map<Integer, Double> prices = new HashMap<>();

        double basePrice = session.getEvent().getPrice();
        String rating = session.getEvent().getRating();
        if (rating.equalsIgnoreCase("high")) {
            basePrice *= RATE_HIGH_RATING;
        }

        AuditoriumEntity auditorium = session.getAuditorium();
        int seatsCount = auditorium.getCount();
        String vips = auditorium.getVipseats();

//        HashSet<String> vipSeats = new HashSet<>(Arrays.asList(vips.split("[, ]")));
        Set<Integer> setIntegerPrice = parseVips(vips);

        for (int i = 0; i < seatsCount; i++) {
            if (setIntegerPrice.contains(i + 1)) {
                prices.put(i + 1, basePrice * RATE_VIP);
            } else {
                prices.put(i + 1, basePrice);
            }
        }
        return prices;
    }

    private Set<Integer> parseVips(String vips) {
        return Stream.of(vips.split("[, ]"))
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
    }
}
