package com.epam.ttb7.service;

import com.epam.ttb7.entity.EventEntity;

import java.util.Collection;
import java.util.Date;

public interface EventService {

    EventEntity saveEvent(EventEntity event);
    void removeEvent(EventEntity event);
    EventEntity getEventById(int id);
    EventEntity getEventByName(String name);
    Iterable<EventEntity> getEvents();

    Collection<EventEntity> getEventsForDateRange(Date from, Date to);
    Collection<EventEntity> getNextEvents(Date to);

//    Event getEventByName(String name);
//    List<Event> getAllEvenet();
//    List<Event> getEventsForDateRange(Date from, Date to);
//    List<Event> getNextEvents(Date to);

}
