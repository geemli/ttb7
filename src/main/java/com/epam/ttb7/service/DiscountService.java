package com.epam.ttb7.service;


import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.entity.UserEntity;

import java.math.BigDecimal;
import java.util.Map;

public interface DiscountService {
//    double getDiscount(User user, Event event, Date dateTime, int numberOfTickets);

    BigDecimal getTicketsPrice(UserEntity user);
    Double getTicketRatePrice(TicketEntity ticket);

    Map<Integer, Double> getTicketsPrice(SessionEntity session);
}
