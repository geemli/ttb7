package com.epam.ttb7.service;

import com.epam.ttb7.dto.TicketDto;
import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.entity.UserEntity;
import com.epam.ttb7.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeMap;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private DiscountService discountService;

    @Override
    public Iterable<TicketEntity> getTickets(SessionEntity session) {
        return ticketRepository.findBySession(session);
    }

    @Override
    public Iterable<TicketEntity> getPurchasedTickets(UserEntity userEntity) {
        return ticketRepository.findByPurchasedTrueAndUserIs(userEntity);
    }

    @Override
    public Iterable<TicketEntity> getPurchasedTickets() {
        return ticketRepository.findByPurchasedTrueAndUserIs(userService.getCurrentUser());
    }

    @Override
    public Iterable<TicketEntity> getBookingTickets(UserEntity userEntity) {
        return ticketRepository.findByPurchasedFalseAndUserIs(userEntity);
    }

    @Override
    public Iterable<TicketEntity> getBookingTickets() {
        return ticketRepository.findByPurchasedFalseAndUserIs(userService.getCurrentUser());
    }

    @Override
    public Iterable<Integer> getSeats(SessionEntity session) {
        return ticketRepository.selectSeatsBySession(session);
    }

    @Override
    public TicketEntity save(TicketEntity ticket) {
        return ticketRepository.save(ticket);
    }

    @Override
    public void bookingTickets(String seats, String sessionId) {
        String[] seatsArr = seats.split("[, ]");
        SessionEntity currentSession = sessionService.getSession(Integer.parseInt(sessionId));


        UserEntity currentUser = userService.getCurrentUser();
        TicketEntity ticket;
        for (String seat : seatsArr) {
            ticket = new TicketEntity();
            ticket.setPurchased(false);
            ticket.setSeat(Integer.parseInt(seat));
            ticket.setSession(currentSession);
            ticket.setUser(currentUser);
            ticket.setPrice(discountService.getTicketRatePrice(ticket));
            save(ticket);
        }
    }

    @Override
    public Map<Integer, TicketDto> getTicketsMap(String sessionId) {
        int sessionInt = Integer.parseInt(sessionId);
        SessionEntity session = sessionService.getSession(sessionInt);
        int seatCount = session.getAuditorium().getCount();

        Map<Integer, Double> ticketsPrice = discountService.getTicketsPrice(session);

        Map<Integer, TicketDto> seats = new TreeMap<>();
        for (int i = 0; i < seatCount; i++) {
            Double currentTicketPrice = ticketsPrice.get(i + 1);
            if(currentTicketPrice == null) {
                throw new RuntimeException();
            }
            seats.put(i + 1, new TicketDto(true, currentTicketPrice));
        }

        //если уже куплены, то билеты становтся недоступными
        Iterable<Integer> available = getSeats(sessionService.getSession(sessionInt));
        for (Integer i : available) {
//            TicketDto newTicket = seats.
//            seats.replace(i, false);
            seats.get(i).setAvailable(false);
        }
        return seats;
    }
}
