package com.epam.ttb7.service;

import com.epam.ttb7.entity.EventEntity;
import com.epam.ttb7.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Date;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public EventEntity saveEvent(EventEntity event) {
        return eventRepository.save(event);
    }

    @Override
    public void removeEvent(EventEntity event) {
        eventRepository.delete(event);
    }

    @Override
    public EventEntity getEventById(int id) {
        return eventRepository.findOne(id);
    }

    @Override
    public EventEntity getEventByName(String name) {
        return eventRepository.findByNameIgnoreCase(name);
    }

    @Override
    public Iterable<EventEntity> getEvents() {
        return eventRepository.findAll();
    }

    @Override
    public Collection<EventEntity> getEventsForDateRange(Date from, Date to) {
        return eventRepository.getDatesEvents(from, to);
    }

    @Override
    public Collection<EventEntity> getNextEvents(Date to) {
        return eventRepository.getDatesEvents(new Date(), to);
    }

    @PostConstruct
    public void init() {
        EventEntity event1 = new EventEntity();
        event1.setName("star wars");
        event1.setDuration(120);
        event1.setRating("HIGH");
        event1.setPrice(7D);

        EventEntity event2 = new EventEntity();
        event2.setName("lord of the ring");
        event2.setDuration(130);
        event2.setRating("HIGH");
        event2.setPrice(6D);



        EventEntity event3 = new EventEntity();
        event3.setName("harry potter");
        event3.setDuration(140);
        event3.setRating("HIGH");
        event3.setPrice(4D);

        eventRepository.save(event1);
        eventRepository.save(event2);
        eventRepository.save(event3);
    }
}
