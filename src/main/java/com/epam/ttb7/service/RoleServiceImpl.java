package com.epam.ttb7.service;

import com.epam.ttb7.entity.RoleEntity;
import com.epam.ttb7.entity.UserEntity;
import com.epam.ttb7.repository.RoleRepository;
import com.epam.ttb7.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public RoleEntity findByName(String role) {
        return roleRepository.findByNameIgnoreCase(role);
    }

    @Override
    public Iterable<RoleEntity> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Iterable<RoleEntity> getRolesForUserRegister(UserEntity user) {
        boolean isAdmin = false;

        if (user != null && user.getRoles() != null) {
            isAdmin = user.getRoles().stream().anyMatch(role -> role.getName().equalsIgnoreCase("ROLE_ADMIN"));
        }

        if (isAdmin) {
            return findAll();
        } else {
            Set<RoleEntity> roles = new HashSet<>();
            roles.add(findByName("ROLE_USER"));
            return roles;
        }
    }
}
