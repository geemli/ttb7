package com.epam.ttb7.service;

import com.epam.ttb7.dto.RegisterUserDto;
import com.epam.ttb7.entity.UserEntity;

public interface UserService {
    UserEntity getCurrentUser();
    UserEntity registerUser(UserEntity user);
    UserEntity registerUser(RegisterUserDto user);
    UserEntity findUserByEmail(String email);
    UserEntity findUserByLogin(String login);
    void removeUser(UserEntity user);
    Iterable<UserEntity> getAllUsers();

}
