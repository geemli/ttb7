package com.epam.ttb7.service;

import com.epam.ttb7.dto.TicketDto;
import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.entity.UserEntity;

import java.util.Map;

public interface TicketService {

    Iterable<TicketEntity> getTickets(SessionEntity session);

    Iterable<TicketEntity> getPurchasedTickets(UserEntity userEntity);
    Iterable<TicketEntity> getPurchasedTickets();

    Iterable<TicketEntity> getBookingTickets(UserEntity userEntity);
    Iterable<TicketEntity> getBookingTickets();

    //    @Query("select te.seat from TicketEntity te where te.session = :session")
    Iterable<Integer> getSeats(SessionEntity session);

    TicketEntity save(TicketEntity ticket);
    void bookingTickets(String seats, String sessionId);
    Map<Integer, TicketDto> getTicketsMap(String sessionId);
}
