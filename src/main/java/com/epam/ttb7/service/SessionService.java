package com.epam.ttb7.service;

import com.epam.ttb7.entity.SessionEntity;

public interface SessionService {

    SessionEntity addSession(SessionEntity sessionEntity);
    Iterable<SessionEntity> getSessions();
    SessionEntity getSession(int id);
}
