package com.epam.ttb7.service;

import com.epam.ttb7.dto.RegisterUserDto;
import com.epam.ttb7.entity.UserEntity;

import com.epam.ttb7.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private RoleService roleService;

    @Override
    public UserEntity findUserByEmail(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

    @Override
    public UserEntity findUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public UserEntity getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return findUserByLogin(auth.getName());
    }

    @Override
    public UserEntity registerUser(UserEntity user) {
        String password = user.getPassword();
        user.setPassword(bCryptPasswordEncoder.encode(password));
        return userRepository.save(user);
    }

    @Override
    public UserEntity registerUser(RegisterUserDto user) {
        return registerUser(convertUserDtoToEntity(user));
    }

    @Override
    public void removeUser(UserEntity user) {
        userRepository.delete(user);
    }

    @Override
    public Iterable<UserEntity> getAllUsers() {
        return userRepository.findAll();
    }

    private UserEntity convertUserDtoToEntity(RegisterUserDto dto) {
        return new UserEntity(dto.getLogin(), dto.getPassword(), dto.getName(), dto.getEmail(), dto.getBirthday(), dto.getRoles());
    }
}
