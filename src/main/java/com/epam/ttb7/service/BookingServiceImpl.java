package com.epam.ttb7.service;

import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    TicketRepository ticketRepository;



    @Override
    public TicketEntity bookTicket(TicketEntity ticket) {
        return ticketRepository.save(ticket);
    }

    @Override
    public Iterable<TicketEntity> getPurchasedTickets(SessionEntity session) {
        return ticketRepository.findBySession(session);
    }

    private Set<Integer> parseVips(String vips) {
        return Stream.of(vips.split("[, ]"))
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
    }
}
