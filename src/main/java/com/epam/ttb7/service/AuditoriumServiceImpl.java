package com.epam.ttb7.service;

import com.epam.ttb7.entity.AuditoriumEntity;
import com.epam.ttb7.repository.AuditoriumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@PropertySource({"classpath:auditorium.properties",})
public class AuditoriumServiceImpl implements AuditoriumService {

    @Autowired
    private AuditoriumRepository auditoriumRepository;

    @Autowired
    Environment env;

    @PostConstruct
    public void insertDB() {
        AuditoriumEntity auditorium = new AuditoriumEntity();
        auditorium.setName(env.getProperty("name"));
        auditorium.setCount(Integer.parseInt(env.getProperty("number")));
        auditorium.setVipseats(env.getProperty("vip"));
        auditoriumRepository.save(auditorium);
    }

    @Override
    public Iterable<AuditoriumEntity> getAllAuditoriums() {
        return auditoriumRepository.findAll();
    }

    @Override
    public AuditoriumEntity getByName(String name) {
        return auditoriumRepository.findByNameIgnoreCase(name);
    }

    private List<Integer> vipsToList() {
        if (env.getProperty("vips") == null) {
            return null;
        }
        return Arrays.stream(env.getProperty("vips").split(",")).map(Integer::valueOf).collect(Collectors.toList());
    }
}
