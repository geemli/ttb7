package com.epam.ttb7.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "AUDITORIUM")
public class AuditoriumEntity {
    private Integer idauditorium;
    private String name;
    private Integer count;
    private String vipseats;
    private String address;
    private String owner;
    private String mobile;
    private Collection<SessionEntity> sessions;

    @Id
    @Column(name = "IDAUDITORIUM")
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Integer getIdauditorium() {
        return idauditorium;
    }

    public void setIdauditorium(Integer idauditorium) {
        this.idauditorium = idauditorium;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "COUNT")
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "VIPSEATS")
    public String getVipseats() {
        return vipseats;
    }

    public void setVipseats(String vipseats) {
        this.vipseats = vipseats;
    }

    @Basic
    @Column(name = "ADDRESS")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "OWNER")
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "MOBILE")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditoriumEntity that = (AuditoriumEntity) o;
        return Objects.equals(idauditorium, that.idauditorium) &&
                Objects.equals(name, that.name) &&
                Objects.equals(count, that.count) &&
                Objects.equals(vipseats, that.vipseats) &&
                Objects.equals(address, that.address) &&
                Objects.equals(owner, that.owner) &&
                Objects.equals(mobile, that.mobile);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idauditorium, name, count, vipseats, address, owner, mobile);
    }

    @OneToMany(mappedBy = "auditorium")
    public Collection<SessionEntity> getSessions() {
        return sessions;
    }

    public void setSessions(Collection<SessionEntity> sessions) {
        this.sessions = sessions;
    }
}
