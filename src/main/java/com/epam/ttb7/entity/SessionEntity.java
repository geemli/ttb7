package com.epam.ttb7.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;


import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "SESSION")
public class SessionEntity {
    private Integer idsession;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime datetime;

    private EventEntity event;
    private AuditoriumEntity auditorium;
    private Collection<TicketEntity> tickets;

    @Id
    @Column(name = "IDSESSION")
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Integer getIdsession() {
        return idsession;
    }

    public void setIdsession(Integer idsession) {
        this.idsession = idsession;
    }

    @Basic
    @Column(name = "DATETIME")
    public LocalDateTime  getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime  datetime) {
        this.datetime = datetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionEntity that = (SessionEntity) o;
        return Objects.equals(idsession, that.idsession) &&
                Objects.equals(datetime, that.datetime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idsession, datetime);
    }

    @ManyToOne
    @JoinColumn(name = "EVENT_IDEVENT", referencedColumnName = "IDEVENT")
    public EventEntity getEvent() {
        return event;
    }

    public void setEvent(EventEntity event) {
        this.event = event;
    }

    @ManyToOne
    @JoinColumn(name = "AUDITORIUM_IDAUDITORIUM", referencedColumnName = "IDAUDITORIUM")
    public AuditoriumEntity getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(AuditoriumEntity auditorium) {
        this.auditorium = auditorium;
    }

    @OneToMany(mappedBy = "session")
    public Collection<TicketEntity> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<TicketEntity> tickets) {
        this.tickets = tickets;
    }
}
