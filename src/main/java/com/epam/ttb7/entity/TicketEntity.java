package com.epam.ttb7.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "TICKET")
public class TicketEntity {

    private Integer idticket;
    private String description;
    private Integer seat;
    private UserEntity user;
    private SessionEntity session;
    private boolean purchased;
    private Double price;

    @Id
    @Column(name = "IDTICKET")
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Integer getIdticket() {
        return idticket;
    }

    public void setIdticket(Integer idticket) {
        this.idticket = idticket;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "SEAT")
    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    @Basic
    @Column(name = "PRICE")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "PURCHASED")
    public boolean isPurchased() {
        return purchased;
    }

    public void setPurchased(boolean purchased) {
        this.purchased = purchased;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketEntity that = (TicketEntity) o;
        return Objects.equals(idticket, that.idticket) &&
                Objects.equals(description, that.description) &&
                Objects.equals(seat, that.seat);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idticket, description, seat);
    }

    @ManyToOne
    @JoinColumn(name = "USER_IDUSER", referencedColumnName = "IDUSER")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "SESSION_IDSESSION", referencedColumnName = "IDSESSION")
    public SessionEntity getSession() {
        return session;
    }

    public void setSession(SessionEntity session) {
        this.session = session;
    }
}
