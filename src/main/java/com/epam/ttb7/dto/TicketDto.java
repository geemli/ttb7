package com.epam.ttb7.dto;

public class TicketDto {
    boolean available;
    double price;

    public TicketDto() {
    }

    public TicketDto(boolean available, double price) {
        this.available = available;
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
