package com.epam.ttb7.dto;

public class SessionDto {

    private Integer sessionId;
    private String seats;

    public SessionDto() {
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }
}
