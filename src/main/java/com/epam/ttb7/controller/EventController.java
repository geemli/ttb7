package com.epam.ttb7.controller;

import com.epam.ttb7.entity.EventEntity;
import com.epam.ttb7.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@Controller
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping("/add_event")
    public String addEvent(Model model) {
        Set<String> ratings = new HashSet<>();
        ratings.add("low");
        ratings.add("mid");
        ratings.add("high");
        model.addAttribute("ratings", ratings);
        return "/add_event";
    }

    @PostMapping(value = "/add_event")
    public String postAddEvent(Model model,
                               @RequestParam("name") String name,
                               @RequestParam("price") String price,
                               @RequestParam("rating") String rating,
                               @RequestParam("duration") String duration) {

        Set<String> errors = new TreeSet<>();

        if (isNullOrEmpty(name)) {
            errors.add("fill the field name");
        }
        if (!name.matches("[a-zA-Z]+")) {
            errors.add("incorrect name");
        }

        if (isNullOrEmpty(price)) {
            errors.add("fill the field price");
        }
        if (!price.matches("\\d+")) {
            errors.add("wrong price");
        }


        Double numberPrice = null;
        try {
            numberPrice = Double.parseDouble(price);
        } catch (NumberFormatException e) {
            errors.add("price is not a number");
        }

        if (isNullOrEmpty(rating)) {
            errors.add("fill the field rating");
        }
        if (!rating.matches("low|mid|high")) {
            errors.add("wrong rating");
        }

        if (isNullOrEmpty(duration)) {
            errors.add("fill the field duration");
        }
        if (!duration.matches("\\d+")) {
            errors.add("wrong duration");
        }

        Integer numberDuration = null;
        try {
            numberDuration = Integer.parseInt(price);
        } catch (NumberFormatException e) {
            errors.add("duration is not a number");
        }

        if (!errors.isEmpty()) {
            model.addAttribute("errors", errors);
            return "/add_event";
        }

        EventEntity newEvent = new EventEntity(name, numberPrice, rating, numberDuration);
        eventService.saveEvent(newEvent);
        return "/events";
//        return "redirect:/events";
    }

    @GetMapping("events")
    public String events(Model model) {
        Iterable<EventEntity> events = eventService.getEvents();
        if (events.spliterator().getExactSizeIfKnown() == 0) {
            model.addAttribute("message", "No events to show");
        } else {
            model.addAttribute("events", events);
        }
        return "/events";
    }

    private boolean isNullOrEmpty(String param) {
        return param == null || param.isEmpty();
    }
}