package com.epam.ttb7.controller;

import com.epam.ttb7.dto.RegisterUserDto;
import com.epam.ttb7.entity.UserEntity;
import com.epam.ttb7.service.RoleService;
import com.epam.ttb7.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @GetMapping("/register")
    public String register(Model model) {
        UserEntity currentUser = userService.getCurrentUser();
        model.addAttribute("roles", roleService.getRolesForUserRegister(currentUser));
        model.addAttribute("user", new RegisterUserDto());
        return "/register";
    }

    @PostMapping("/register")
    public String postRegister(@Valid @ModelAttribute("user") RegisterUserDto user, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()) {
            UserEntity currentUser = userService.getCurrentUser();
            model.addAttribute("roles", roleService.getRolesForUserRegister(currentUser));
            return "/register";
        }

        if(userService.findUserByLogin(user.getLogin()) != null) {
            bindingResult.rejectValue("login", "user.login.exists","user with this login already exists");
            return "/register";
        }
        if(userService.findUserByEmail(user.getEmail()) != null) {
            bindingResult.rejectValue("email", "user.email.exists","user with this email already exists");
            return "/register";
        }

        userService.registerUser(user);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login() {
        return "/login";
    }

    @GetMapping("/login_wrong")
    public String wrongLogin() {
        return "/login_wrong";
    }

    @GetMapping("/menu")
    public String menu() {
        if(userService.getCurrentUser().getRoles().contains("ROLE_ADMIN")) {
            return "/admin";
        } else {
            return "/user";
        }
    }
}
