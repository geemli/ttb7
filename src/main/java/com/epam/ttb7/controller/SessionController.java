package com.epam.ttb7.controller;

import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.service.AuditoriumService;
import com.epam.ttb7.service.EventService;
import com.epam.ttb7.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.text.SimpleDateFormat;

@Controller
public class SessionController {

    @Autowired
    private EventService eventService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private AuditoriumService auditoriumService;

    @GetMapping("add_session")
    public String addSession(Model model) {
        model.addAttribute("session2", new SessionEntity());
        model.addAttribute("events", eventService.getEvents());
        model.addAttribute("auditories", auditoriumService.getAllAuditoriums());
        return "/add_session";
    }

    @PostMapping("add_session")
    public String postAddSession(@Valid @ModelAttribute("session2") SessionEntity session2, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "error/403";
        }
        sessionService.addSession(session2);
        return "redirect:/sessions";
    }

    @GetMapping("sessions")
    public String sessions(Model model) {
        model.addAttribute("sessions",  sessionService.getSessions());
        model.addAttribute("localDateTimeFormat", new SimpleDateFormat("yyyy-MM-dd'T'hh:mm"));
        return "/sessions";
    }
}
