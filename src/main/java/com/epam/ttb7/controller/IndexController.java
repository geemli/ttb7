package com.epam.ttb7.controller;

import com.epam.ttb7.service.AuditoriumService;
import com.epam.ttb7.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @Autowired
    AuditoriumService auditoriumService;

    @Autowired
    UserService userService;

    @GetMapping({"/", "/home"})
    public String home1() {
        return "/home";
    }

    @GetMapping("/admin")
    public String admin() {
        return "/admin";
    }

    @GetMapping("/user")
    public String user() {
        return "/user";
    }

    @GetMapping("/tickets")
    public String about() {
        return "/tickets";
    }



    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }

}
