package com.epam.ttb7.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ExceptionHandlingController {

    @ExceptionHandler(Exception.class)
    public String handleError(HttpServletRequest req, Model model) {
        return "error/403";
    }

    @GetMapping("/error")
    public String errorPage(HttpServletRequest req, Model model) {
        return "error/403";
    }
}
