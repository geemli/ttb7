package com.epam.ttb7.controller;

import com.epam.ttb7.dto.SessionDto;
import com.epam.ttb7.dto.TicketDto;
import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.entity.UserEntity;
import com.epam.ttb7.service.AuditoriumService;
import com.epam.ttb7.service.BookingService;
import com.epam.ttb7.service.DiscountService;
import com.epam.ttb7.service.SessionService;
import com.epam.ttb7.service.TicketService;
import com.epam.ttb7.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.TreeMap;

@Controller
public class TicketController {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private UserService userService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private AuditoriumService auditoriumService;

    @Autowired
    private DiscountService discountService;


    @GetMapping("/view_purchased_tickets")
    public String viewPurchasedTickets(Model model) {
        model.addAttribute("tickets", ticketService.getPurchasedTickets());
        return "view_purchased_tickets";
    }

    @GetMapping("/choice_session")
    public String choiceSession(Model model) {
        model.addAttribute("sessions", sessionService.getSessions());
        model.addAttribute("session", new SessionDto());
        return "/choice_session";
    }

    @GetMapping("/choice_session/{session}")
    public String postChoiceSession(@PathVariable("session") String session, Model model) {
//        SessionEntity sessionEntity = sessionService.getSession(Integer.parseInt(session));
//        int seatCount = sessionEntity.getAuditorium().getCount();
//        model.addAttribute("seatsCount", seatCount);

        Map<Integer, TicketDto> ticketsMap = ticketService.getTicketsMap(session);

        model.addAttribute("availableSeats", ticketsMap);
//        model.addAttribute("vipSeats", auditoriumService.getByName(sessionEntity.getAuditorium().getVipseats()));
        model.addAttribute("sessionId", session);
        return "/choice_seat";
    }

    @PostMapping("/choice_seat")
    public String postChoiceSeat(@RequestParam(value = "seats") String seatsString , @RequestParam(value = "sessionId") String sessionId) {

        ticketService.bookingTickets(seatsString, sessionId);
        return "/user";
    }

    @GetMapping("/cart")
    public String cart(Model model) {
        UserEntity user = userService.getCurrentUser();
        model.addAttribute("tickets", ticketService.getBookingTickets(user));
        model.addAttribute("price", discountService.getTicketsPrice(user));
//        model.addAttribute("cost", bookingService.getTicketsPrice(user));
        return "/cart";
    }

    @PostMapping("/buy_tickets")
    public String buyTickets(Model model) {
        //TODO: дважды приходится лезть в базу. Передать коллекцию на jsp и обратно...
        UserEntity user = userService.getCurrentUser();
        Iterable<TicketEntity> tickets = ticketService.getBookingTickets(user);

        for (TicketEntity ticket : tickets) {
            ticket.setPurchased(true);
            ticketService.save(ticket);
        }

        model.addAttribute("price", discountService.getTicketsPrice(user));
        return "redirect:/user";
    }
}
