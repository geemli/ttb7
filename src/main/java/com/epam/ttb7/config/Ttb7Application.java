package com.epam.ttb7.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@SpringBootApplication(scanBasePackages = "com.epam.ttb7")
@EnableJpaRepositories("com.epam.ttb7.repository")
@EntityScan(value = "com.epam.ttb7.entity")
@EnableTransactionManagement

public class Ttb7Application {

	public static void main(String[] args) {
		SpringApplication.run(Ttb7Application.class, args);
	}
}
