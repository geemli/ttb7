package com.epam.ttb7.repository;

import com.epam.ttb7.entity.EventEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;

@Repository
public interface EventRepository extends CrudRepository<EventEntity, Integer> {
    EventEntity findByNameIgnoreCase(String name);


    //    @Query("select e from EventEntity where e.id between ?1 and ?2")

    @Query("select e from EventEntity e " +
            "inner join e.sessions s " +
            "where s.datetime between ?1 and ?2")
    Collection<EventEntity> getDatesEvents(Date from, Date to);

}
