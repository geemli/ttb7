package com.epam.ttb7.repository;

import com.epam.ttb7.entity.SessionEntity;
import com.epam.ttb7.entity.TicketEntity;
import com.epam.ttb7.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends CrudRepository<TicketEntity, Integer> {
    Iterable<TicketEntity> findBySession(SessionEntity session);
    Iterable<TicketEntity> findByPurchasedTrueAndUserIs(UserEntity userEntity);
    Iterable<TicketEntity> findByPurchasedFalseAndUserIs(UserEntity userEntity);

    @Query("select te.seat from TicketEntity te where te.session = :session")
    Iterable<Integer> selectSeatsBySession(@Param("session") SessionEntity session);

}
