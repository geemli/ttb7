package com.epam.ttb7.repository;

import com.epam.ttb7.entity.AuditoriumEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditoriumRepository extends CrudRepository<AuditoriumEntity, Integer> {
    AuditoriumEntity findByNameIgnoreCase(String name);
}
