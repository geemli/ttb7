package com.epam.ttb7.repository;

import com.epam.ttb7.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findByEmailIgnoreCase(String email);
    UserEntity findByLogin(String login);
    Optional<UserEntity> findOneWithAuthoritiesWithUserInfoByLogin(String login);
}
