<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Choice session</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Auditory</th>
            <th scope="col">Date Time</th>
            <th scope="col">Price</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="currentSession" items="${sessions}">
            <tr>
                    <%--<form:radiobutton path="sessionId" value="${currentSession.idsession}"/>--%>
                <%--<input type="hidden" value="${session.sessionId}"/>--%>
                <%--<td>--%>
                        <%--${currentSession.idsession}--%>
                <%--</td>--%>
                <td>
                        ${currentSession.event.name}
                </td>
                <td>
                        ${currentSession.auditorium.name}
                </td>
                <td>
                        ${currentSession.datetime}
                </td>
                <td>
                    <a class="btn btn-secondary" href="/choice_session/${currentSession.idsession}">Buy</a>
                </td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>