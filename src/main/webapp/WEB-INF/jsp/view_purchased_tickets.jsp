<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Sessions</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Seat</th>
            <th scope="col">Name</th>
            <th scope="col">Date&Time</th>
            <th scope="col">Event</th>
            <th scope="col">Auditorium</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="ticket" items="${tickets}">
            <tr>
                    <%--<c:out value="${ticket.id}"></c:out><br>--%>
                <th><c:out value="${ticket.seat}"/></th>
                <th><c:out value="${ticket.user.name}"/></th>
                <th><c:out value="${ticket.session.datetime}"/></th>
                <th><c:out value="${ticket.session.event.name}"/></th>
                <th><c:out value="${ticket.session.auditorium.name}"/></th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
