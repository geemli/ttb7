<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Login page</title>
    <%@ include file="fragments/header.jsp" %>
    <%--${pageContext.request.userPrincipal.name}--%>
</head>
<body>

<div class="container">
    <div class="error">
        <c:forEach var="error" items="${errors}">
            <c:out value="${error}"/><br>
        </c:forEach>
        <br><br>
    </div>

    <form action="/login" method="post">
        <fieldset>
            <h1>Please Sign In</h1>
            <div class="form-group">
                <label for="username">Login</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Login">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>
            <input type="submit" class="btn btn-primary" value="Sign In"/>
        </fieldset>
        <a href="/register">Register</a>

    </form>


    <%--<div class="row" style="margin-top:20px">--%>
    <%--<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">--%>
    <%--<form action="login" method="post">--%>
    <%--<fieldset>--%>
    <%--<h1>Please Sign In</h1>--%>

    <%--<div class="form-group">--%>
    <%--<input type="text" name="username" id="username" class="form-control input-lg"--%>
    <%--placeholder="UserName" required="true" autofocus="true"/>--%>
    <%--</div>--%>
    <%--<div class="form-group">--%>
    <%--<input type="password" name="password" id="password" class="form-control input-lg"--%>
    <%--placeholder="Password" required="true"/>--%>
    <%--</div>--%>

    <%--<div class="row">--%>
    <%--<div class="col-xs-6 col-sm-6 col-md-6">--%>
    <%--<input type="submit" class="btn btn-lg btn-primary btn-block" value="Sign In"/>--%>
    <%--</div>--%>
    <%--<div class="col-xs-6 col-sm-6 col-md-6">--%>
    <%--</div>--%>
    <%--</div>--%>
    <%--</fieldset>--%>
    <%--</form>--%>
    <%--<a href="/register">Register</a>--%>
    <%--</div>--%>
    <%--</div>--%>
    <%--<c:if test="${not empty pageContext.request.userPrincipal.name}">--%>
    <%--${pageContext.request.userPrincipal.name}--%>
    <%--<a href="<c:url value="/logout" />">Logout</a>--%>
    <%--</c:if>--%>
</div>

<%@ include file="fragments/footer.jsp" %>

</body>
</html>
