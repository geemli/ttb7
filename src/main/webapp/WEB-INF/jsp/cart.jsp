<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>User cart</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">
    <form:form action="/buy_tickets" method="post" modelAttribute="tickets">

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Seat</th>
            <th scope="col">Date&Time</th>
            <th scope="col">Event</th>
            <th scope="col">Price</th>
            <th scope="col">Duration(in min.)</th>
            <th scope="col">Rating</th>
            <th scope="col">Auditory</th>
            <th scope="col">Auditory capacity</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="ticket" items="${tickets}">
            <tr>
                <td>${ticket.seat}</td>
                <td>${ticket.session.datetime}</td>
                <td>${ticket.session.event.name}</td>
                <td>${ticket.price}</td>
                <td>${ticket.session.event.duration}</td>
                <td>${ticket.session.event.rating}</td>
                <td>${ticket.session.auditorium.name}</td>
                <td>${ticket.session.auditorium.count}</td>
            </tr>
        </c:forEach>

        <tr>
            <td>
                Total count include discount: <h1>${price}</h1>
            </td>
        </tr>
        <tr>
            <td>
                <form:button type="submit">buy tickets</form:button>
            </td>
        </tr>
        </form:form>
        </tbody>
    </table>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
