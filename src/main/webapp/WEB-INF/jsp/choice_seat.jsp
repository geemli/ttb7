<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Choice seat</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">
    <form method="post" action="/choice_seat">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Seat</th>
                <th scope="col">Price</th>
                <th scope="col">Booking</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="seat" items="${availableSeats}">
            <c:if test="${seat.value.available}">
            <tr>
                <td>${seat.key}</td>
                <td>${seat.value.price}</td>
            </tr>
            </c:if>
            <c:if test="${not seat.value.available}">
            <tr class="table-danger">
                <td>${seat.key}</td>
                <td>buyed</td>
            </tr>
            </c:if>

            </c:forEach>
        </table>
        <input name="seats"/>
        <input name="sessionId" type="hidden" value="${session}"/>
        <button type="submit">submit</button>
    </form>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>