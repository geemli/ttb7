<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
<link href="/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/css/sticky-footer.css" rel="stylesheet"/>
<link href="/css/main.css" rel="stylesheet"/>


<%--https://bootswatch.com/cerulean/--%>
<%--TODO: https://bootswatch.com/cerulean/ --%>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
            aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/login">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/register">Register</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item active">
                <div class="nav-link">
                    <c:if test="${not empty pageContext.request.userPrincipal}">
                        Logged user: ${pageContext.request.userPrincipal.name} <br>
                    </c:if>
                    <c:if test="${empty pageContext.request.userPrincipal}">
                        Guest<br>
                    </c:if>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item active">
                <div class="nav-link">
                    <c:if test="${not empty pageContext.request.userPrincipal}">
                        Roles:
                        <sec:authorize access="hasRole('ROLE_USER')"> user </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')"> admin <br/></sec:authorize>
                    </c:if>
                </div>
            </li>
        </ul>
        <%--</div>--%>
        <c:if test="${not empty pageContext.request.userPrincipal}">
            <a href="/logout" class="btn btn-secondary my-2 my-sm-0" type="submit">Log out</a>
        </c:if>
    </div>
</nav>