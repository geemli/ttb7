<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Events</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>
<div class="container">
    <h6>${message}</h6>

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <%--<th scope="col">Price</th>--%>
            <th scope="col">Rating</th>
            <th scope="col">Duration(in min.)</th>
        </tr>
        </thead>

        <c:forEach items="${events}" var="event">
            <tr>
                <td>${event.name}</td>
                    <%--<td>${event.price}</td>--%>
                <td>${fn:toUpperCase(event.rating)}</td>
                <td>${event.duration}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
