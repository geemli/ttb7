<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Sessions</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Date&Time</th>
            <th scope="col">Event</th>
            <th scope="col">Auditorium</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach items="${sessions}" var="session">
            <tr>
                <th>${session.event.name}</th>
                <th>${session.auditorium.name}</th>
                <th>${localDateTimeFormat.parse(session.datetime)}</th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
