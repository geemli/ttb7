<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>403</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">
    <img src="/image/xCH4ODP.gif" alt="cheetah in the rain" >
    <div class="starter-template">
        <h1>403 - Access is denied</h1>
    </div>

</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
