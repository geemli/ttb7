<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Sessions</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>
<div class="container">

    <div class="starter-template">

        <h2><a href="/events">view events</a></h2>
        <h2><a href="/cart">my shopping cart</a></h2>
        <h2><a href="/tickets_price">get tickets price</a></h2>
        <h2><a href="/choice_session">choice ticket</a></h2>

    </div>

</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
