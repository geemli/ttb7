<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Add new session</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">

    <form:form action="/add_session" method="post" modelAttribute="session2">

        date & time
        <form:input type="datetime-local" path="datetime"/>

        <%--<form:radiobuttons itemLabel="name" path="event" items="${events}" />--%>
        <br>
        <c:forEach var="item" items="${events}">
            <form:radiobutton path="event" value="${item.idevent}"/>
            ${item.name} name
            ${item.price} price
            ${item.rating} rating
            ${item.duration} duration
        </c:forEach>

        <br>
        <c:forEach var="item" items="${auditories}">
            <form:radiobutton path="auditorium" value="${item.idauditorium}"/>
            ${item.name}
            ${item.count}
            ${item.vipseats}
        </c:forEach>


        <form:button type="submit">submit</form:button>
    </form:form>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
