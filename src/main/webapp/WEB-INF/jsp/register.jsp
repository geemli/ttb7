<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Register new user</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">
    <form:form action="/register" method="post" modelAttribute="user">
                     
        <div class="form-group">
            <form:label class="col-form-label" path="login">Login</form:label>
            <form:input type="text" class="form-control" placeholder="input login…" path="login" required="required"/>
            <form:errors cssClass="error_RED" path="login"/>
        </div>

        <div class="form-group">
            <form:label class="col-form-label" path="password">Password</form:label>
            <form:input type="text" class="form-control" placeholder="input password…" path="password" required="required"/>
            <form:errors cssClass="error_RED" path="password"/>
        </div>

        <div class="form-group">
            <form:label class="col-form-label" path="name">Name</form:label>
            <form:input type="text" class="form-control" placeholder="input name…" path="name" required="required"/>
            <form:errors cssClass="error_RED" path="name"/>
        </div>

        <div class="form-group">
            <form:label class="col-form-label" path="email">E-mail</form:label>
            <form:input type="text" class="form-control" placeholder="input email…" path="email" required="required"/>
            <form:errors cssClass="error_RED" path="email"/>
        </div>

        <div class="form-group">
            <form:label class="col-form-label" path="birthday">Birthday</form:label>
            <form:input type="date" class="form-control" placeholder="choice birthday…" path="birthday" required="required"/>
            <form:errors cssClass="error_RED" path="birthday"/>
        </div>

        <div class="form-group">
            <c:forEach var="role" items="${roles}">
                <form:checkbox path="roles" value="${role.idrole}"/>
                <c:out value="${role.name}"/>
            </c:forEach>
            <form:errors cssClass="error_RED" path="roles"/>
        </div>
        <div class="form-group">
            <form:button class="btn btn-primary" type="submit">Register</form:button>
        </div>
    </form:form>
    <a href="/login">Already have account</a>
</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
