<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Add new event</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="error">
    <c:forEach var="error" items="${errors}">
        <c:out value="${error}"/><br>
    </c:forEach>
    <br><br>
</div>

<div class="container">
    <div class="form-group">

        <form action="add_event" method="post" class="form-control">
            <fieldset>
                <legend>Add event</legend>
                <div class="form-group">
                    <label for="name">name</label>
                    <input class="form-control" type="text" name="name" id="name" required/>
                </div>
                <div class="form-group">
                    <label for="price">price</label>
                    <input class="form-control" type="number" name="price" id="price" required/>
                </div>
                <div class="form-group">
                    <label for="rating">rating</label>
                    <select class="form-control" id="rating" name="rating">
                        <option value="low">low</option>
                        <option value="mid">middle</option>
                        <option value="high">high</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="duration">duration</label>
                    <input class="form-control" type="number" name="duration" id="duration" required/><br>
                </div>
                <input class="btn btn-primary" type="submit" value="Submit"/>
                <input class="btn btn-primary" type="reset" value="Reset"/>
            </fieldset>
        </form>
    </div>

</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
