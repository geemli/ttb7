<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Admin panel</title>
    <%@ include file="fragments/header.jsp" %>
</head>
<body>

<div class="container">

    <a href="/add_event">add event</a> <br/>
    <a href="/events">view events</a> <br/>

    <a href="/add_session">add session</a> <br/>
    <a href="/sessions">view sessions</a> <br/>

    <%--<a href="/edit_user">edit user</a> <br/>--%>
    <a href="/view_purchased_tickets">view purchased tickets</a>

</div>
<%@ include file="fragments/footer.jsp" %>

</body>
</html>
